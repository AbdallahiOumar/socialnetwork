{% load account %}{% user_display user as user_display %}{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain %} {{ site_name }}!

Vous reçoyez cette e-mail parceque user {{ user_display }} a utiliser votre e-mail pour connecter compte.

Pour confirmer, Allez à {{ activate_url }}
{% endblocktrans %}
{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Merci de notre part {{ site_name }}!
{{ site_domain }}{% endblocktrans %}
{% endautoescape %}
