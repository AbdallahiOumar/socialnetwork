from django import forms
from .models import Post, Comment

class PostForm(forms.ModelForm):
    body = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={
            'rows': '4',
            'placeholder': 'Poster ici...............'
            }))
    Image = forms.FileField(required=False)

    class Meta:
        model = Post
        fields = ['body', 'Image']

class CommentForm(forms.ModelForm):
    comment = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={
            'rows': '3',
            'placeholder': 'Commenter ici..........'
            }))

    class Meta:
        model = Comment
        fields = ['comment']

class ShareForm(forms.Form):
    body = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={
            'rows': '2',
            'placeholder': '......'
            }))
